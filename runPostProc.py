#!/usr/bin/env python

######################################
# To submit ZdZdPostProcessing jobs over all data or background samples easier
# and in a controlled manner 
#
#   run as:     python runPostProc.py
# 
#   
#
######################################

#import sys          # for command line arguments
import os           # to find all the files in the directory that we are considering
import datetime     # so that we know the time
import subprocess   # to call commands from the command line
from collections import defaultdict # to easier declare a dict of lists here
import argparse # to parse command line options

parser = argparse.ArgumentParser()
parser.add_argument("pathToNtuples",nargs='*', help="specify the directory that contains the nTuples we want to process")

parser.add_argument("--dsidSelector", type = str, nargs='*', default = None,
     help="if this is defined, we consider _only_ the sample that corresponds to this sample")

parser.add_argument("--split", type = float, default = -1,
     help="sets the split for the ganga submission. I.e. in how many subjet we wanna split a given DSID")


parser.add_argument("--joboptions", type = str, default = "ZXPlottingAlgJobOptionsSyst.py",
     help="sets which joboptions file we use")


#parser.add_argument("--walltime", type = str, default = "8nh", choices = ["8nm", "1nh", "8nh", "1nd", "2nd", "1nw", "2nw"],
#     help="sets the split for the ganga submission. I.e. in how many subjet we wanna split a given DSID")

parser.add_argument("--outputDirSuffix", type = str, default = "",
     help="set a suffix for the ouput directory, i.e. data will be saved in ./post_<date>_<time><outputDirSuffix>")

args = parser.parse_args()

pathToNtuples = args.pathToNtuples

################################## prepare list of samples ##################################

fileCounter = 0
groups = defaultdict(list) # save list of samples here, {dsid_descriptor : listOfFiles}

for sampleDir in args.pathToNtuples:
    for root, dirs, files in os.walk( sampleDir ): # from the 'root' dir, walk through all the sub-'dirs' and gives a list of files in 'files'
        for file in files:
            #
            if '.root' not in file: continue # skip files that are not root files

            filenameComponents = file.split('.')

            if len(filenameComponents) == 9: 
                user, username, dsid, sampleDescription, tags, gridjob, tupletype, partnum, extension = filenameComponents
            elif len(filenameComponents) == 6:
                user, username, gridjob, tupletype, partnum, extension = filenameComponents
                dsid = "000000"
                sampleDescription = "data"
                tags = "e0000_s0000__r0000_p0000"
            
            #if dsidFilder is set and the current sample id is _not_ in the filter list, disregard that sample
            if args.dsidSelector != None and dsid not in args.dsidSelector:  print("DSID " + dsid + " skipped"); continue

            key = dsid + "_" + sampleDescription
            abspath = os.path.join(root, file)
            groups[key].append(abspath)
            fileCounter += 1


# Decide which split we want to rquest
if args.split <= 0 : splitSetting  = fileCounter  # negative number, request one job per file
elif args.split < 1 : splitSetting = int(fileCounter * args.split) # 
else: splitSetting = int(args.split)

for key in groups: # print the DSIDs that we have for the benefit of the user
    print key, "\t N files: ", len(groups[key])

################# create a unique output folder (by using timestamp as name) #################

now = datetime.datetime.now()
date = now.strftime("%Y%m%d_%H%M%S")

folder = "post" + "_" + date + args.outputDirSuffix
print "Making folder: ", folder

if not os.path.exists(folder):
    os.makedirs(folder)

########################## create text files for ganga submission ##########################
# we put the complete path to all files for a given DSID into a file
# one file per DSID for the ganga submission
# if we put all samples (regardless of DSID) into one file and pass it to ganga, 
# it will treat them as belonging together
# but we don't want this, because we want to be able to seperate them after the ganga is done

sampleSubmissionFiles = []
with open(folder + "/in_all.txt", "w") as allDSIDtxt:
    for key in groups:
        print "Generating list of input ntuples for Ganga: ", key
        with open(folder + "/in_" + key + ".txt", "w") as currentDSIDtxt:
            print folder
            for ntuple in groups[key]:
                currentDSIDtxt.write(ntuple + '\n')
                allDSIDtxt.write(ntuple + '\n')
            sampleSubmissionFiles.append("'" + os.path.realpath(currentDSIDtxt.name) + "'")

infiles = ",".join(sampleSubmissionFiles) # joing list of paths together to a string, so we can pass it to ganga
print infiles


###############################  create ganga output location ###############################
outputLocation = os.path.realpath(folder) + "/out"
if not os.path.exists(outputLocation):
    os.makedirs(outputLocation)
print "We will put the putput files here: " + outputLocation



############################# prepare and execute ganga command #############################

print "---XXX---"
#                                                                --split=1 do this for now, will automatically incraesed to the number of different DSIDs
#gangacmd = "ganga athena --lsf --queue {} --inPfnListFile='{}' --split={} --outputdata='histos.root' --outputlocation='{}'".format(args.walltime, infiles,args.split, outputLocation) + " ZdZdPlotting/ZdZdPlottingAlgJobOptions.py"
gangacmd = "ganga athena --condor --inPfnListFile='{}' --split={} --outputdata='histos.root' --outputlocation='{}'".format(infiles, splitSetting , outputLocation) + " ZdZdPlotting/" + args.joboptions
#                      (do lsf for LXPlus) --local for other cluster
print gangacmd

runScript = os.path.realpath(folder) + "/runGanga_" + folder + ".sh"
with open(runScript, "w") as b:
    b.write('#!/bin/bash\n')
    b.write('set -euxo pipefail\n')  # some things to make our shell schript fail more gracefully
    b.write('printenv\n')
    b.write(gangacmd)

os.chmod(runScript, 0755)

subprocess.call(runScript) # run the script

###### non-ganga submissions ###
###
###for key in groups:
###    print "Starting post-processing on: ", key
###
###    filesInput = ",".join(groups[key])
###
###    outputFile = "hist_" + key
###    print "-------------------"
###
###    folderabspath = os.path.abspath(folder)
###    print folderabspath
###    subprocess.call(["athena.py", "ZdZdPlotting/ZdZdPlottingAlgJobOptions.py", "--filesInput={}".format(filesInput), "-c outputFile=\'{}.root\';".format(outputFile)], cwd=folderabspath)







# import pdb; pdb.set_trace()   ## enter here with the debugger
