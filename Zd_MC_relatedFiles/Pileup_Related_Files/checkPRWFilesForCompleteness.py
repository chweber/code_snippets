# run as: python checkPRWFilesForCompleteness.py

# Author: Christian Weber

#########################################################################
#   This script checks if the present PRW (pileup reweighting) files are complete for the given sample files
#   The location of the PRW files and the .txt files with the sample names is currently harcoded
#   Will print mc subcampaign and DSID of incomplete samples
#########################################################################





import os # to do things with the file system



################# define locations of PRW files and location as well as names of our sample lists #################

# define locations for the PRW files (relative from where this scrip is)
localPRW_FileLocations = {"mc16a" : "mc16a", 
                          "mc16d" : "mc16d",
                          "mc16e" : "mc16e"}

# parth to the location where the sample lists are stored relative to the location of this python file
pathToTheSampleListLocation = ".."

# define what are the text files where we store the name of the samples
mcSampleLists = { "mc16a" : ["bkg_datasets_mc16a.txt", "bkg_datasets_mc16a_extra.txt", "signal_datasets_mc16a.txt"] ,
                  "mc16d" : ["bkg_datasets_mc16d.txt", "bkg_datasets_mc16d_extra.txt", "signal_datasets_mc16d.txt"] ,
                  "mc16e" : ["bkg_datasets_mc16e.txt", "bkg_datasets_mc16e_extra.txt"]}


# make sure that we use the same keys in the dicts 'localPRW_FileLocations' and 'mcSampleLists'
assert localPRW_FileLocations.keys() == mcSampleLists.keys(), 'Not a consistent set of dict-keys used in \"localPRW_FileLocations\" \"and mcSampleLists\"'




################# Set up the latest release and other things that we need to run the checkPRW.py script #################
# see here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting#Checking_PRW_Config_files

# This is now quite working, executing these commands from python. So we are shifting this to a bash script, which calls this python script

## setupThePRW_Environemnt = False
##     
## if setupThePRW_Environemnt: #Set up the AthAnalysis environment here so we can run the PRW scropts
##     os.system("acmSetup AthAnalysis,21.2,latest")   # set up the latest release
##     os.system("lsetup pyAMI panda")                 # set up pyAMI so that the PRW scripts can access AMI
##     os.system("voms-proxy-init -voms atlas")        # set up a access to the grid






################# Do a PRW Checking #################



# cycle over our mc subcampaigns, e.g. mc16a, mc16d, etc
for mcRelease in localPRW_FileLocations.keys():

    # where our PRW files for our subcampaign, e.g. mc16d/*.root
    currentPRW_Files = localPRW_FileLocations[mcRelease] +"/*.root"

    # cycle over our lists of samples for our subcampaign, e.g. bkg_datasets_mc16d.txt, signal_datasets_mc16d.txt
    for sampleSet in mcSampleLists[mcRelease]:

        # define path to sample list
        currentSampleList = pathToTheSampleListLocation + "/" + sampleSet

        # this is the command line that would lead to the checking of prw samples, e.g. checkPRW.py --inDsTxt=../signal_datasets_mc16d.txt mc16d/*.root
        checkPRWLine = "checkPRW.py --inDsTxt="+ currentSampleList +" "+ currentPRW_Files

        # define the file that we will use to pipe shell outputs to and to parse them from, e.g. prwStatus_bkg_datasets_mc16d.txt
        prwCheckOutFile = "prwStatus_" + sampleSet

        # we need to alter the way the buffering for the piping is done so that the piping workd - the reasons for that are note entirely clear to me
        bufferPrefix = "stdbuf -oL -eL " 


        print("run the PRW checking: " + currentSampleList) ## run the PRW checking

        # now the the actual checking of PRW files, pipe the output to a file
        os.system(bufferPrefix + checkPRWLine +" > "+ prwCheckOutFile)


        ########### parse the output file ###########

        # open the output file
        prwOutput = open(prwCheckOutFile,'r')

        # loop over all the lines in it
        for line in prwOutput:

            # if a given DSID reports as imcomplete alert the user. There may also be 'suspect' prw files. We believe for now that they are benign 
            if 'incomplete' in line:
                print(line)




print("All done!")

#import pdb; pdb.set_trace()