#!/bin/bash

# run as: . combineAndMoveRootPRWFiles.sh

## let's loop over all subdirectories, 
## combine the rootfiles in each subdirectory that start with NTUP_PILEUP*
## and name the combined file like the foldername, but prepend it with PRWFILE_
## move the combined root file to the directory where the bash script was executed


## remember the directory where we started. We wanna move the combined root files here

CURRENTDIR=$PWD



## loop over subdirectories
for DIRECTORY in */ ; do

	# switch to the given subdirectory
	cd $DIRECTORY

	# remove the last charactor from the directory name (which is a \ in this case and disturbs the hadd function)
	DIRECTORY_MINUS_SLASH=${DIRECTORY%?}

	# define the name for the combined root files
	HADD_FILENAME="PRWFILE_$DIRECTORY_MINUS_SLASH.root"

	# combine the root files
	hadd $HADD_FILENAME NTUP_PILEUP*

	# move the combined root files backto the super-directory
	mv $HADD_FILENAME "$CURRENTDIR/"

	# go back to the 'superdirectory'
	cd ..

	# remove that directory and the contents within
	rm -r $DIRECTORY
    
done


##To cycle through through (sub) directory
#for DIRECTORY in */ ; do
#	echo "$DIRECTORY"
#done

##To cycle through files in directory
#for FILE in *; do
#	echo "$FILE"
#done