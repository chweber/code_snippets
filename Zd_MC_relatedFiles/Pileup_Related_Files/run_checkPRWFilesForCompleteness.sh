#!/bin/bash

# run as: . run_checkPRWFilesForCompleteness.sh

## let's set up the environments that we need to do the PRW checking and generating and do the checks of all PRW files for all samples



acmSetup AthAnalysis,21.2,latest
lsetup pyAMI panda	
voms-proxy-init -voms atlas


python checkPRWFilesForCompleteness.py