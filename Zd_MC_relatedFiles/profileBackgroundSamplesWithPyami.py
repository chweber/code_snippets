##############################################
# 
# profileBackgroundSamplesWithPyami.py
#
# requires python version >2.7 :  lsetup python
# requires pyami:                 lsetup pyami
# and ATLAS voms-proxy :          voms-proxy-init -voms atlas
# 
#
# 
# For a gicen MC sample, this script check pyAmi for versions of the sample with different p-tags
# Prints out these samples in a file, together with the number of events in each of them
# 
# 
# run as: 
# python profileBackgroundSamplesWithPyami.py  sampleList1 sampleList2 
# profile all background samples:
# python profileBackgroundSamplesWithPyami.py  bkg_datasets_mc16*.txt
# 
#
# to look for the mc16e equivalents of mc16d samples, and list number of events
# python profileBackgroundSamplesWithPyami.py bkg_datasets_mc16d* --forceMCTag mc16e --listNEvents True
#
# to check if there are mc16e versions of the samples in the mc16d sample list
# python profileBackgroundSamplesWithPyami.py bkg_datasets_mc16d*.txt --forceMCTag mc16e
#
#
# check if the samples in the lists have sufficiently recent p-tags (without querying AMI at all)
# python profileBackgroundSamplesWithPyami.py  bkg_datasets_mc16*.txt --pTagNewerThan 3517 --skipAMI True
# 
#
# Author: Christian Weber
##############################################




import re # to do regular expression matching
import subprocess   # for executing bash commands
import argparse # to parse command line options
import sys 
import warnings
from collections import Counter # to count instances in a list later on, to spot instances of double listing samples


###################### check python versions ######################

pythonVersion = sys.version_info # output is some tuple

if pythonVersion[0]+float(pythonVersion[1])/10  < 2.7:
    sys.exit("Python version needs to be > 2.7 \nVersion you have is: " 
        + str(pythonVersion[0]) +"."+ str(pythonVersion[1])+"."+ str(pythonVersion[2]) +
        "\nLoad antoher version of python. Maybe try \'lsetup python\' ")



#################### setup commandline options ####################

parser = argparse.ArgumentParser()  # set up the command line parser
parser.add_argument("fileToCheck", nargs='*', type=str, 
    help = "name of the files listing samples that we wanna check against in pyAMI. Accepts multiple entries") 
parser.add_argument("--forceMCTag", type=str, choices=["mc16a","mc16d","mc16e"] , default=False,
    help = "usually we parse the MC Tag from the file names, but we can also force a specific one for any reason" ) 
parser.add_argument("--listNEvents", type=bool, default=False,
   help = "Add this command line argument to also list the number of events in the found sample" ) 
parser.add_argument("--searchSampleWithLatestPTag", type=bool, default=False,
   help = "Set this command line argument to True search for the exact sample with the latest pTag" ) 
parser.add_argument("--pTagNewerThan", type=int, default=-1,
   help = "Set this command line argument to some int to check if the samples in the list (not AMI) have a p-Tag newer than the int. e.g. --pTagNewerThan 3516" ) 
parser.add_argument("--skipAMI", type=bool, default=False,
   help = "Set this command to True to skip the part of the scrip that queries AMI, usefull if using --pTagNewerThan XYZ" ) 
args = parser.parse_args() # make the parser parse the arguments



for sampleFileName in args.fileToCheck: # loop over all the file names that have been appended in the command line

    # define MC tags, so that we can only search for the correct MC sub-campaign
    if not args.forceMCTag :
        if "mc16a" in sampleFileName:   mcCampaignTag = "r9364_r9315"
        elif "mc16d" in sampleFileName: mcCampaignTag = "r10201_r10210"
        elif "mc16e" in sampleFileName: mcCampaignTag = "r10724_r10726" #r10726
    else:
        mcTagDict = {"mc16a":"r9364_r9315", "mc16d":"r10201_r10210" , "mc16e":"r10724_r10726"}
        mcCampaignTag = mcTagDict[args.forceMCTag]

    print( "start processing: " + sampleFileName)
    sampleFile = open(sampleFileName,'r') # open the file that contains the list of MC samples

    outputfileName = sampleFileName + "_DSID_Profile.Out"  # name of the file where we wanna save the output

    # if we have a mc sample listing samples from mc epoch X but want the equivalents of mc epoch Y make this explicit in the output file by adding the forces MCTag to the file name
    if args.forceMCTag : outputfileName += args.forceMCTag

    outputFile = open(outputfileName, "w") # w for (over) write

    sampleList = []
    for line in sampleFile: # parse all the lines in the file with the samples
        if line.startswith("#"): continue   # skip commented lines

        DSIDRegExpression = re.search("\d{6}", line) # # DSIDs are numbers 6 digits long,  use search to look for the DSID anywehere in the string, instead of 'match' that looks only at the beginning

        #print(DSIDRegExpression)
        if DSIDRegExpression: #if the line contains a DSID with out pattern

            sampleList.append(line) # gather all the lines in a list, to check eventually if we have double entries

            lineWithoutNewLineCharacter = line.split("\n")[0] #remove the new line character that ends the line
            DSID = DSIDRegExpression.group()

            # check that we do not have samples with the wrong release in the list:
            releaseTagRegExpression = re.search("r(\d{5}|\d{4})_r(\d{5}|\d{4})", line) # match 5 or 4 digits -- (\d{5}|\d{4})

            if releaseTagRegExpression is None : warnings.warn( "releaseTagRegExpression is none. Check whati s going on"); import pdb; pdb.set_trace()

            if (not args.forceMCTag) and (releaseTagRegExpression.group() != mcCampaignTag): print( "Wrong release tag! DSID "+ DSID + " has wrong release tag. Should be " + mcCampaignTag+ " but is " + releaseTagRegExpression.group() )

            #lineWithoutNewLineCharacter = lineWithoutNewLineCharacter.split()[0] # remove any whitespace ## superfluous there should only be one white space, and that is the one at the end of the line, that we already tool care of
            derivationType = lineWithoutNewLineCharacter.split(".")[4] # look for the Derivation Type, in our case it should be DAOD_HIGG2D1

            allTags = lineWithoutNewLineCharacter.split(".")[-1]
            allTagsExcept_pTag = "_".join(allTags.split("_")[0:-1])
            pTag = allTags.split("_")[-1]
            pTagNumber = int(pTag[1:])

            # check if p-tag is sufficiently 
            if pTagNumber <  args.pTagNewerThan : print( DSID + " has p-Tag lower than " + str( args.pTagNewerThan ))

            ###################### do the AMI things ######################
            if args.skipAMI: continue 

            if args.searchSampleWithLatestPTag:  searchTags = allTagsExcept_pTag # if we wanna find the exact sample with just the latest p-tag
            else: searchTags = mcCampaignTag # if we wanna find the mcY equivalent of the given sample


            amiCommand = ["ami", "list","datasets", "--type",derivationType  , "%" +DSID+"%"+searchTags+"%"]
            if args.listNEvents :  amiCommand.extend( ["--fields", "events"]) # if we choose so, add the command to inquire about the number of events in the sample

            #--fields events
            pyAmiResult = subprocess.check_output( amiCommand )


            if lineWithoutNewLineCharacter not in pyAmiResult: 
                if not args.listNEvents: print(lineWithoutNewLineCharacter + "\t not found in Ami") # avoid that message if we are also listing the number of events in each sample
                else: print(lineWithoutNewLineCharacter)
                allSamplesFoundInAmi = False
                currentSampleFound = False
            else: 
                print(DSID + " ok!")
                currentSampleFound = True

            #pyAmiResultList = pyAmiResult.split("\n")
            #for result in pyAmiResultList: print(result)



            if args.searchSampleWithLatestPTag: 
                outputFile.write( pyAmiResult.split("\n")[-2] + "\n")
            else:
                outputFile.write( "\n\n\n-------------"+DSID+"-------------   " + " ".join(amiCommand) + "\n")
                outputFile.write( line+ "--------------------------\n"  )
                outputFile.write(pyAmiResult); 



            #import pdb; pdb.set_trace()

            #pyamiString = "ami list datasets --fields events --type " +derivationType+ " %"+DSID+"%"+mcCampaignTag+"%"  

            #delimiterString = "echo -------------"+DSID+"-------------   "

            #subprocess.Popen(delimiterString  + pyamiString + pipeString, shell=True).wait() # pipe string to file that described what DSID we are profiling and what inquiry we are sending to pyami
            #subprocess.Popen("echo "+lineWithoutNewLineCharacter+ pipeString, shell=True).wait() # pipe the name of the sample that we currently started of to the output file
            ##subprocess.Popen("echo "+ pyamiString + pipeString, shell=True).wait()
            #subprocess.Popen(pyamiString + pipeString, shell=True).wait() # pipe the pyAMI output to the file
            #subprocess.Popen("echo "+pipeString, shell=True).wait()

            ##subprocess.Popen(pyamiString, shell=True).wait()
            #import pdb; pdb.set_trace()

        # check if you listed any samples more than once
        countedSampleList = Counter(sampleList)
        for sample in countedSampleList: 
            if countedSampleList[sample] > 1 : print( "The following sample is listed more than once sample file: " + sample)

    outputFile.close()

            


    print("Done! Check " + outputfileName)

        

        
# import pdb; pdb.set_trace()