
## run as:  python scanRootFile.py


############################################################################################
# 
#   Opens a root file, and a tree in that root file (optionally in a folder)
#   and prints its content by using the TTree.Scan() method
# 
############################################################################################

import ROOT     # to do all the ROOT stuff
import argparse # to parse command line options

# the part of the code that executes if I call this script directly
if __name__ == '__main__':

    ############################################
    # setup command line options
    ############################################
    parser = argparse.ArgumentParser()

    parser.add_argument("RootFile", type=str, help="The .root file to open")

    parser.add_argument("TTree", type=str, help="The TTree that we want to open")

    parser.add_argument("-v", "--scanVariables", type=str, 
        help="Specific list if variables/branches that we want to scan. E.g.: \"var1:var2:var3\" ")

    parser.add_argument("-d", "--TDirectory", type=str, help="The TDirectory inside the TFile that contains the TTree")

    args = parser.parse_args()




    ############################################
    # setup location of the TTree to scan
    ############################################

    myRootFileName = args.RootFile

    myRootFile = ROOT.TFile(myRootFileName,"READ")


    if args.TDirectory is not None:  myDirectory = myRootFile.GetDirectory( args.TDirectory )
    else:                            myDirectory = myRootFile

    myTree = myDirectory.Get( args.TTree )  

    myTree.GetPlayer().SetScanRedirect(True)
    myTree.GetPlayer().SetScanFileName("scanOut.txt")


    ############################################
    # scan the TTree
    ############################################

    # Long64_t TTree::Scan  (   varexp = "",selection = "",Option_t *  option = "",Long64_t    nentries = kMaxEntries, Long64_t    firstentry = 0 )   

    if args.scanVariables is not None:  myTree.Scan(args.scanVariables,"","colsize=20" )    # Loop over tree entries and print entries passing selection.
    else:                               myTree.Scan("*","","colsize=40",10)                      # Loop over tree entries and print entries passing selection.

    # myTree.Scan("llll_lexochanz_rank:llll_pdgIdSum:l_pdgId:llll_ll1:llll_ll2:ll_l1:ll_l2")

    import pdb; pdb.set_trace()

    #numberOfBranches = myTree.GetEntries()

    ###################################
    #
    #TFile myRootFile("user.ychiu.13370068.NTUP4L._000003.root");
    #
    #myDirectory = myRootFile.GetDirectory("Nominal")
    #
    #A = myRootFile.GetDirectory("Nominal")
    #
    #myTree = A->Get("llllTree")
    #
    ## myTree->Print()
    #
    #
    #TTreeReader myTTreeReader(myTree)
    #
    #
    #
    #
    #