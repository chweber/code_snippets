## run as: python getLatestRootFile.py

############################################################################################
# Ganga outputs its results in a folder structure like
# GangaOut/JOB.SUBJOB/myfile.root
# where JOB and SUBJOB are integers
# 
# Here I want to get all of the .root files belonging to the latest job, 
# combine them to a single root file, and put them in the directory where I call this pything script from.
# Assume that I want the root files from the laters job (i.e. the one with the largest Job integer)
# and I am also assuming I am running this script from the run folder that includes the GangaOut folder.
# 
# I.e. the current working direcotry is /run and the folderstructure is:
# run/GangaOut/JOB.SUBJOB/myfile.root
# 
# the combined rootfile will by default have the name 
# 
# "job"+largestJobNumber+"_ZZd_results.root", e.g. "job40_ZZd_results.root"
# 
############################################################################################


import os # to do things with the file system
import shutil # to copy files
import subprocess # to run bash commands
#import math # for floor
import argparse # to parse command line options


parser = argparse.ArgumentParser()

parser.add_argument("gangaOutDir")

parser.add_argument("-t", "--targetFileName", type=str,
        help="specify root directory in which the objects are we want to compare")

args = parser.parse_args()

pwd = os.getcwd() # remember the present working directory (pwd) so that we can return to it if necessary

# the folder in which all the JOB.SUBJOB folders are
rootFileLocation = args.gangaOutDir

# get a list of JOB.SUBJOB folders
gangaResultDirs = os.listdir(rootFileLocation)

# initialise our variable for the larger job number to something that is smaller than any possible job number
largestJobNumber = 0
# get the larger job number in the rootFileLocation, the dir names are structures as a flot like JOB.SUBJOB
for i in gangaResultDirs: largestJobNumber = max([largestJobNumber,float(i)])

# we want the largestJobNumber as a str,but split it at the decimal point, and keep only the part left of it
largestJobNumber =                  str(largestJobNumber).split(".")[0] # i.e. keep the "40" part of "40.9"

print("\nLatest job is nr " + largestJobNumber +"\n")

# save the directories that correspond to our choosen job here
relevantResultDirs = []

# pick the directors that correspont to our choosen job
for directory in gangaResultDirs:
    if str(largestJobNumber) in directory.split(".")[0]: relevantResultDirs.append(directory)


# create directory for temorarely storing the root files
if not os.path.exists("tempDir"): os.makedirs("tempDir")

# copy  the pertinent root files into the temp directory
for i in relevantResultDirs: 
    shutil.copyfile("GangaOut/"+i+"/histos.root", "tempDir/myfile"+i+".root")

# change to the temp dir so we can combine the root files there
os.chdir("tempDir")

# set a name for the combined root file
if args.targetFileName is None:  combinedRootFileName = "job"+largestJobNumber+"_ZZd_results.root"
else: combinedRootFileName = args.targetFileName

#actually combine the root files
os.system("hadd "+combinedRootFileName+" *.root")

# go back to the initial direcotry
os.chdir("..")

# move the root file that we just created
os.rename("tempDir/"+combinedRootFileName,combinedRootFileName)

# delete the tempDir and the files within
shutil.rmtree("tempDir") # will delete a directory and all its contents




#os.remove() #will remove a file.
#os.rmdir("tempDir")# will remove an empty directory.
#os.system("rm tempDir/*.root")



#import pdb; pdb.set_trace()
