import ROOT
import re # to do regular expression matching


def getTTreeAndItsKey(TFile, TTreeName = None ):
    # assume the TTree that we want to move is in the TFile, and not in some sub-TDirectory of it
    # TTreeName is a string and the name of the TTree that we are interested in
    # if no TTreeName is specified, we will take the first TTree that we can find (maybe change it later to all TTrees)

    for tKey in TFile.GetListOfKeys():

        myTTreeCandidate = tKey.ReadObj() # get the current tObject
        # make sure we have the correct type
        if not isinstance(myTTreeCandidate , ROOT.TTree): continue

        if TTreeName is not None: # check TTree name if we have specified one
            if myTTreeCandidate.GetName() != TTreeName: continue

        return myTTreeCandidate, tKey # we got what we came for. no need to iterate more

    return False, False

    

def getUniqueDSID(TFile, rePatternDSID = "\d{6}"):
    # parse the name of all TObjects in the base level of the TFile (i.e. no sub direcotires)
    # surch occurences of the 6 consecutive digits (i.e. the shape of an DSID), and check that the DSID is unique
    listOfNames = []

    for tKey in TFile.GetListOfKeys():  listOfNames.append(tKey.GetName() )


    jointNames = ",".join(listOfNames) # join the individual names such that we can parse them at one

    reOutput = re.findall(rePatternDSID, jointNames ) # use find all to look for all instances of the 


    if len(set(reOutput)) != 1 : raise ValueError( "DSID is not unique" ) # DSID is not unique
    else: 
        DSID = reOutput[0] # DSID is unique, so we can pick any element
        return DSID



def moveTTree(fileLocation, TTreeName = None, targetDir = None):

    myTFile = ROOT.TFile.Open(fileLocation,'Update')

    myTTree, oldTTreeKey = getTTreeAndItsKey(myTFile, TTreeName)


    if not myTTree : myTFile.Close(); return True # if we didn't find an TObject with our desired name, close the file and go on



    if targetDir is None: 
        DSID = getUniqueDSID(myTFile)
        targetDir=DSID+"/Nominal" # where we want to move the TTree

    else: myTFile.mkdir(targetDir)

    myTFile.cd(targetDir) # switch to that direcotry

    newTTree = myTTree.CloneTree() # clone the TTree, we'll delete the original one shortly
    newTTree.Write()

    myTFile.cd()

    oldTTreeKey.Delete()

    myTFile.Close()

    return True




# the part of the code that executes if I call this script directly
if __name__ == '__main__':

    moveTTree("histos.root", TTreeName = None)
    
    #import pdb; pdb.set_trace()
