########################################################################################
#
# PyROOT script to hadd (i.e. combine) root files that are distributed in sub and subsub (etc)
# directories of a given parent direcoty
# 
#
# Author: Christian Weber
########################################################################################
#

import os           # to find all the files in the directory that we are considering
import subprocess   # for executing bash commands
import argparse # to parse command line options
from moveTTree import moveTTree 


######################################################
# This is the function that does the actual work
# Wo thrawl recursively through all the subdirs of the parent directory
# and store all the absolute paths to all the root files
# and than use the command line "hadd" to produce a combined .root file
######################################################

def getListOfRootFiles(parentDir,useStrictEnding):

    listOfFiles = []    # put the sbsolute paths to the .root file we find here

    #go through directories and subdirectories
    for dirpath, dirnames, files in os.walk(parentDir):
        for file in files: 

            if useStrictEnding: includeFile = file.endswith(".root") 
            else:               includeFile = ".root" in file

            if includeFile:
                abspath = os.path.join(dirpath, file)
                listOfFiles.append(abspath)

    return listOfFiles


def assembleFilesToCombine(parentDir, combinedRootFileName = "combinedRoot.root", useStrictEnding = True, doTTreeMove = False, TTreeName = None):
    # useStrictEnding - checks if the filename ends with ".root",
    #                   if False, check if the filename contains ".root" instead


    listOfFiles = getListOfRootFiles(parentDir,useStrictEnding)
    if doTTreeMove:  # move top-level TTree to <DSID>\Nominal
        for fileName in listOfFiles: moveTTree(fileName, TTreeName = TTreeName)



    n = 1000 # maxNumberOfFilesPerHADDAttempt
    
    while len(listOfFiles) > n:
        sublist = listOfFiles[0:n]
        del listOfFiles[0:n]
        combineDistributedRootFiles(sublist,combinedRootFileName = "tempCombinedRootOut.root")

        os.rename("tempCombinedRootOut.root", "tempCombinedRootIn.root")

        listOfFiles = ["tempCombinedRootIn.root"] + listOfFiles

    combineDistributedRootFiles(listOfFiles ,combinedRootFileName = combinedRootFileName)

    if os.path.isfile("tempCombinedRootIn.root"): os.remove("tempCombinedRootIn.root")


def combineDistributedRootFiles(listOfFiles ,combinedRootFileName = "combinedRoot.root"):
    filesToCombine = " ".join(listOfFiles)
    subprocess.Popen("hadd " + combinedRootFileName +" " + filesToCombine, shell=True).wait()# run the hadd command
    return combinedRootFileName



# the part of the code that executes if I call this script directly
if __name__ == '__main__':


    ######################################################
    #Parse Command line options
    ######################################################

    parser = argparse.ArgumentParser()

    parser.add_argument("directory", type=str,
        help="parent directory which contains the root files to comine. Root files may be in subdirs of the parent direcotry")
    parser.add_argument("-f", "--combinedFilename", type=str, help="name of the combined .root file") #, default="combinedRoot.root"
    parser.add_argument("--useStrictEnding", type=bool, default=True,
    help="checks if the filename ends with '.root', if False, check if the filename contains '.root' instead")
    parser.add_argument("--doTTreeMove", type=bool, default=False, help="move top-level TTree to <DSID>\Nominal")
    parser.add_argument("--TTreeName", type=str, default=None, help="name of the TTree that we want to move to <DSID>\Nominal")

    args = parser.parse_args()

    if args.combinedFilename is None: combinedFilename = args.directory+".root"
    else: combinedFilename = args.combinedFilename

    ######################################################
    # Do the actual root file combining
    ######################################################

    assembleFilesToCombine(args.directory, combinedRootFileName = combinedFilename, 
        useStrictEnding = args.useStrictEnding, doTTreeMove =args.doTTreeMove , TTreeName = args.TTreeName )




# import pdb; pdb.set_trace()