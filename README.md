code_snippets



# combineDistributedROOTFiles.py

PyROOT script to hadd (i.e. combine) root files that are distributed in sub and subsub (etc)
directories of a given parent direcoty


# compareROOTFilesByTObjectHash.py

PyRoot script to compare two ROOT files (ROOT_File_A vs ROOT_File_B)
by comparing hashes of the TOBjects inside
