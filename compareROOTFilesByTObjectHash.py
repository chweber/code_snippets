########################################################################################
#
# PyRoot script to compare two ROOT files (ROOT_File_A vs ROOT_File_B)
# by comparing hashes of the TOBjects inside
#
# Author: Christian Weber
########################################################################################
#
#
#  usage: compareROOTFilesByTObjectHash.py [-h] [-d DIRECTORY] [-v] ROOT_File_A ROOT_File_B
#  
#  positional arguments:
#    ROOT_File_A
#    ROOT_File_B
#  
#  optional arguments:
#    -h, --help            show this help message and exit
#    -d DIRECTORY, --directory DIRECTORY
#                          specify root directory in which the objects are we
#                          want to compare
#    -v, --verbose         increase output verbosity
#


import ROOT # to do all the ROOT stuff
import argparse # to parse command line options

    
def getTObjectTitles(rootFileName, rootTDirectory = None):
    # get a list of TObjects' titles inside the ROOT file or ROOTFile/ROOTDirectory

    myRootFile = ROOT.TFile(rootFileName,"READ")

    # if a specific direcotry was defined, get that specific directory
    if rootTDirectory :     myDirectory = myRootFile.GetDirectory( rootTDirectory )
    # otherwise we will look at the lowest level of the directory
    else:                   myDirectory = myRootFile

    TObjectTitles = [] # save the names ('titles' in ROOT parlance) of the TObjects here

    # get all the 'titles'
    for key in myDirectory.GetListOfKeys():
        aTObject = key.ReadObj()
        TObjectTitles.append(aTObject.GetTitle())

    return TObjectTitles, myRootFile



# the part of the code that executes if I call this script directly
if __name__ == '__main__':


    ######################################################
    #Parse Command line options
    ######################################################

    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--directory", type=str,
        help="specify root directory in which the objects are we want to compare")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")
    parser.add_argument("ROOT_File_A")
    parser.add_argument("ROOT_File_B")

    args = parser.parse_args()


    #######################################################
    # compare the hists
    #######################################################

    # get lists of TObjects and the the opened root files
    listOfTObjects_A, myRootFile_A = getTObjectTitles(args.ROOT_File_A, args.directory)
    listOfTObjects_B, myRootFile_B = getTObjectTitles(args.ROOT_File_B, args.directory)   

    # check that the two lists are identical
    if not listOfTObjects_A == listOfTObjects_B: exit("Different amount of TObjects in the two files/directories")

    if args.directory: # enter the rootDirectory if one was specified
    	myDirectory_A = myRootFile_A.GetDirectory( args.directory )
    	myDirectory_B = myRootFile_B.GetDirectory( args.directory )
    else:	# if not, we just stay in the root files
    	myDirectory_A = myRootFile_A
    	myDirectory_B = myRootFile_B

    allObjectsAreIdentical = True

    # go over the all of the TObjects
    for TObjectName in listOfTObjects_A:
        TROOT_File_A = myDirectory_A.Get(TObjectName)
        TROOT_File_B = myDirectory_B.Get(TObjectName)

        # if we are verbose, print the details of the TObject hashes
        if args.verbose: print(TObjectName + "\t Hash_A = " + str( TROOT_File_A.Hash() ) + 
                                             "\t Hash_B = " + str( TROOT_File_B.Hash() ) +
                                            ",\t hash diff:" + str( TROOT_File_A.Hash() - TROOT_File_B.Hash()) )

        # if we find two distinct TObjects, remember that, and let the user know
        if TROOT_File_A.Hash() != TROOT_File_B.Hash(): 
            allObjectsAreIdentical = False
            print(TObjectName + " is not identical between the two files / directories" )


    if allObjectsAreIdentical: print("Files/directories are identical!")
# END if __name__ == '__main__':
