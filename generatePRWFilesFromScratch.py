###########################################################
#    A script that gnerates a set of PRW files for mc samples from scratch
#    takes a list of sample names as input
#    from scratch means here that it creates a grid job for each sample that produces the prw files
#
#
#    usage of the script:    generatePRWFilesFromScratch.py  <file_with_sample_names.txt>
#
#
#    to ensure that the programm runs correctly, 
#    it is important to have a fully setup AthAnalysis environment in the folder it is being run from
#    
#
#    to do so go through the following steps _before_ running the script
#
#    # generate the necessary folders 
#    mkdir build, source
#    cd build
#
#    #setup the actual environment
#    acmSetup AthAnalysis,21.2,latest
#    source $TestArea/*/setup.sh
#
#    # setup pyami for the generatePRW.py command
#    lsetup pyami
#
#    # setup panda to make gridjobs
#    lsetup panda
#
#    # and get a voms proxy so that have access to the grod
#    voms-proxy-init -voms atlas
#
#    
#
#    
#
###########################################################




import argparse # to parse command line options
import os       # to find all the files in the directory that we are considering\
import re       # to do regular expressions
import subprocess   # for executing bash commands



def assembleSampleList(sampleFileName):
    sampleFile = open(sampleFileName,'r') # open the file that contains the list of MC samples

    sampleList = []

    for line in sampleFile: # parse all the lines in the file with the samples
        if not line.startswith("mc"): continue   # skip commented lines
        lineWithoutNewLineCharacter = line.split("\n")[0] #remove the new line character that ends the line
        sampleList.append(lineWithoutNewLineCharacter)

    return sampleList

def getDSIDFromSample(sample):
    reResults = re.findall(r'.(\d{6})', sample) #find six digits in string preceeded by a period
    return reResults[0]

def getTagsFromSaple(sample):
    tags = sample.split(".")[-1]
    return tags


def makeSingleSampleTextFile( sampleString):
    
    DSID = getDSIDFromSample(sample)

    textFileName = DSID+".txt"

    singleSampleFile=open( textFileName ,'w+') # open the file that contains the list of MC samples
    singleSampleFile.write(sampleString)
    singleSampleFile.close()

    return textFileName

def doSampleGeneration( sampleString):

    textFileName = makeSingleSampleTextFile( sample )

    DSID = getDSIDFromSample(sample)
    tags = getTagsFromSaple(sample)

    jobName = "user.chweber.myPRW__"+DSID+"___"+tags

    bashString = "generatePRW.py --skipNTUP_PILEUP  --inDsTxt="+textFileName+" --outDS="+jobName

    print( "Submitting " + sample)
    subprocess.Popen( bashString , shell=True).wait() # pipe string to file that described what DSID we are profiling and what inquiry we are sending to pyami


    
# the part of the code that executes if I call this script directly
if __name__ == '__main__':


    ############# parse command line arguments

    parser = argparse.ArgumentParser()

    parser.add_argument("--sampleFile", type=str, default="bkg_datasets_mc16_for_prwGENERATION.txt", 
        help="file containing the sample names to be generated")

    args = parser.parse_args()


    ############

    sampleList = assembleSampleList( args.sampleFile )

    # change to the source dir, which we should have set up along when creating a proper athena environment

    os.chdir("build")

    for sample in sampleList:
        doSampleGeneration( sample)


# import pdb; pdb.set_trace()